# 4 Servo Motion Recorder electronics enclosure

3D printable tool-less box intended for using with IDEAA Lab's [4 Servo Motion Recorder](https://ideaalab.com/en/shop/servomotors/controllers/4-servo-motion-recorder-detail)