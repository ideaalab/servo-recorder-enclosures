# 1 Servo Movement Recorder electronics enclosure

3D printable tool-less box intended for using with IDEAA Lab's [1 Servo Movement Recorder](https://ideaalab.com/en/shop/servomotors/controllers/1-servo-movement-recorder-detail)